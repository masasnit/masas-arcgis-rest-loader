# MASAS ArcGIS REST Feature Service Tool

## Software Requirements

* Python v2.7.* (https://www.python.org/)
* Mako v1.0.* (http://www.makotemplates.org/)

## Configuration (config.py)

The configuration takes place in config.py. This is the configuration used by all the tools.

### Settings related to ArcGIS

* portal: Used for Token creation. Should point to arcgis.com (Ex: "https://www.arcgis.com")
* clientId: Used for Token creation.  This is the OAUTH app client id. This will be used by the main application instead of using credentials.
* clientSecret: Used for Token creation.  This is the OAUTH app client secret key. This will be used by the main application instead of using credentials.
* user: The name of the user.  This is only used to Create/Clear the layers.
* pointFeatureLayer: A link to your point feature rest endpoint.  "http://host/instance/rest/services/folderName"
* polygonFeatureLayer: A link to your polygon feature rest endpoint.  "http://host/instance/rest/services/folderName"
* lineFeatureLayer: A link to your line feature rest endpoint.  "http://host/instance/rest/services/folderName"
* adminServices: A link to your admin services. "http://host/instance/rest/admin/services"
* contentEndPoint: The base endpoint to the ArcGIS Content API.  In most cases it should point to "http://www.arcgis.com/sharing/rest/content".

### Settings related to MASAS 

* hubURL: The URL of the hub to be queried (Ex: "https://sandbox2.masas-sics.ca/hub/feed")
* authorURI: Add an author if you want results filtered by a specific author. Leave blank for no filtering on author.
* bbox: Add a bounding box to only query a specific region. (Ex: "-76.3573,44.8849,-72.9983,46.1588" for Ottawa & Montreal area)
* accessCode: Your masas Access Code


## Create Layers

This script will create the Feature service and it's layers, run this command:

`python createLayers.py --name "My_MASAS_Feature_Service" -u AGOLUSERNAME -p AGOLPASSWORD`

## Clear Layers

This script will remove all Features from the layers (Point, Line, Polygon) and clear their caches.

`python clearLayers.py -u AGOLUSERNAME -p AGOLPASSWORD`

## Run the service

To run the service, run this command:

`python cronjob.py`