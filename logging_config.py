import logging, logging.handlers
logging_info = {
    "logfile": "system.log", #log will be stored in same folder as service.
    # .syslogaddress - system log name/location 
    # Platform dependencies: MAC "/var/run/syslog", Linux "/dev/log"
    "syslogaddress": "/var/run/syslog",
    # .loglevel - sets default log level for all handlers (can tailor below for each)
    "loglevel": logging.INFO,
    # Loggers to engage. Choices are: console, file, syslog
    "loggers" : ["console"], #["console","file","syslog"], 
    # .logKEY - display in log text (priniciply for syslog searching) 
    "logKEY": "arcgis" 
}
