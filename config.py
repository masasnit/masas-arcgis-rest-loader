#settings used by cronjob.py

arcgisSettings = {
    "portal": "https://{org}.maps.arcgis.com/",
    "clientId": "",
    "clientSecret": "",
    "user": "{AGOL User Name}",
	"password": "{AGOL Password}",
    "pointFeatureLayer": "https://services6.arcgis.com/{ORG_ID}/arcgis/rest/services/{FEATURE_SERVICE_NAME}/FeatureServer/0/",
    "lineFeatureLayer": "https://services6.arcgis.com/{ORG_ID}/arcgis/rest/services/{FEATURE_SERVICE_NAME}/FeatureServer/1/",
    "polygonFeatureLayer": "https://services6.arcgis.com/{ORG_ID}/arcgis/rest/services/{FEATURE_SERVICE_NAME}/FeatureServer/2/",
    "adminService": "https://services6.arcgis.com/{ORG_ID}/arcgis/rest/admin/services/",
    "adminFeatureService": "https://services6.arcgis.com/{ORG_ID}/arcgis/rest/admin/services/{FEATURE_SERVICE_NAME}/FeatureServer",
    "contentEndpoint": "https://www.arcgis.com/sharing/rest/content"
}

hubSettings = {
    "hubURL": "https://sandbox2.masas-sics.ca/hub/feed", #TODO: strip "/feed" from this?
    "authorURI": "", #blank for no author filter
    "bbox": "",
    #"bbox":"-76.3573,44.8849,-72.9983,46.1588", # Ottawa & Montreal area
    "accessCode":""
}