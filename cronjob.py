# -*- coding: utf-8 -*-
from datetime import datetime
import os
import sys

from MasasFeedGrabber import MasasFeedGrabber
from ArcGISManager import ArcGISManager

#load logging config
logging_config = {}
execfile("logging_config.py", {}, logging_config)

# LOGGING
import logging, logging.handlers
from logging.handlers import RotatingFileHandler


current_dir = os.path.dirname(os.path.abspath(__file__))
# create formatter and add it to the handlers
formatter = logging.Formatter(logging_config["logging_info"]["logKEY"] + ' %(asctime)s - %(levelname)s - %(message)s')

loggers = logging_config["logging_info"]["loggers"]

logger = logging.getLogger(logging_config["logging_info"]["logKEY"])
logger.setLevel( logging_config["logging_info"]["loglevel"])

# file-based log
if "file" in loggers:
    app_dir = os.path.dirname(sys.argv[0])
    log_path = os.path.join(current_dir, logging_config["logging_info"]["logfile"])
    print "Logging to file %s" % log_path

    fh = RotatingFileHandler(log_path, maxBytes=100000, backupCount=10)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

# console log
if "console" in loggers:
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    print "added console log for %s" % logging_config["logging_info"]["logKEY"]
    
# syslog
if "syslog" in loggers:
    # hook up the syslog.
    if "logfile" in logging_config["logging_info"]:
        sl = logging.handlers.SysLogHandler(logging_config["logging_info"]["syslogaddress"])
        sl.setLevel(logging.INFO)
        logger.addHandler(sl)


logger.info(" Starting - logger attached.")

#load logging config
app_config = {}
execfile("config.py", {}, app_config)

#Add loggers to settings
app_config["arcgisSettings"]["logger"] = logger
app_config["hubSettings"]["logger"] = logger

arcgis = ArcGISManager(app_config["arcgisSettings"])

tokenTest = False

while not tokenTest:
    arcgis.GetToken()
    tokenTest = arcgis.VerifyToken()
# end while

grabber = MasasFeedGrabber(arcGISManager = arcgis, settings = app_config["hubSettings"])

dtNowString = datetime.utcnow().isoformat("T")[:19] + 'Z'

grabber.GetEntries()
arcgis.RemoveExpiredEntries()

print "DONE."

