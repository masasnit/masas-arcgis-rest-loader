# -*- coding: utf-8 -*-
import urllib2
from xml.dom.minidom import parse
import json
import urllib

# MasasFeedGrabber Class
# .GetEntries(dtsince)
class MasasFeedGrabber:
    """ """
    
    def __init__(self, arcGISManager, settings = {}):
        
        #TODO: Add exception handling, blank checks (and defaults where blank)
        self.hubURL = settings["hubURL"]
        self.authorURI = settings["authorURI"]
        self.bbox = settings["bbox"]
        self.accessCode = settings["accessCode"]
        self.logger = settings["logger"]
        self.arcGISmanager = arcGISManager
        
        print self.hubURL
        
        return None
    
    def GetEntries(self, dtsince = ""):
        """
        Retrieve entries for the area of interest.
        """

        # Get the ArcGIS/MASAS ids...
        pointIds = self.arcGISmanager.GetIds( self.arcGISmanager.pointFeatureLayer );
        lineIds = self.arcGISmanager.GetIds( self.arcGISmanager.lineFeatureLayer );
        polygonIds = self.arcGISmanager.GetIds( self.arcGISmanager.polygonFeatureLayer );

        addPointFeatures = []
        updatePointFeatures = []

        addLineFeatures = []
        updateLineFeatures = []

        addPolygonFeatures = []
        updatePolygonFeatures = []

        if(dtsince != ""):
            hubParams = urllib.urlencode({"dtsince":dtsince,"secret":self.accessCode})
        elif (self.bbox != ""):
            hubParams = urllib.urlencode({"bbox":self.bbox,"secret":self.accessCode})
        else:
            hubParams = urllib.urlencode({"secret":self.accessCode})

        queryURL = self.hubURL +"?"+ hubParams
        self.logger.debug("Hub URL: %s" % queryURL) 
        self.logger.debug("params %s" % hubParams)
        
        xml = urllib2.urlopen(queryURL) #,hubParams)
        dom = parse(xml)
        entry = dom.getElementsByTagName('entry')
        
        for e in entry:
            #self.logger.debug("have entry")
            #get the updated time
            updatedList = e.getElementsByTagName('updated')
            updated = str(updatedList[0].childNodes[0].nodeValue)
            # self.logger.debug("MAX 20, updated is: " + str(len(updated)))
            #find the icon used and insert it
            iconList = e.getElementsByTagName('category')
            icon = ""
            severity = ""
            urgency = ""
            for il in iconList :
                if "Icon" in il.attributes['label'].value:
                    icon =  str(il.attributes['term'].value)
                    if (len(icon) > 40):
                        icon = icon[:40]
                if "Severity" in il.attributes['label'].value:
                    severity =  str(il.attributes['term'].value)
                    if (len(severity) > 40):
                        severity = severity[:40]
                if "Urgency" in il.attributes['label'].value:
                    urgency = str(il.attributes['term'].value)
                    if (len(urgency) > 40):
                        urgency = urgency[:40]
                    # self.logger.debug("MAX 40, icon is: " + str(len(icon)))
            #find the title
            titleList = e.getElementsByTagName('title')
            #TODO: Handle non ASCII (e.g. French) characters.
            #TODO: Allow longer Title value in layers 
            title = titleList[0].childNodes[0].childNodes[0].childNodes[0].nodeValue.encode('ascii', 'ignore')
            # self.logger.debug("MAX 40, title is: " + str(len(title)))
            if (len(title) > 40):
                title = title[:40]
            #find the name
            authorList = e.getElementsByTagName('author')
            name = str(authorList[0].childNodes[0].childNodes[0].nodeValue)
            if (len(name) > 40):
                name = name[:40]

            uriElement = authorList[0].getElementsByTagName('uri')
            uri = str(uriElement[0].childNodes[0].nodeValue)
            if (len(uri) > 256):
                uri = uri[:256]
            # For now, keep the uri to empty since the legacy services can't be modified.
            uri = ""
            
            idList = e.getElementsByTagName('id')
            id = str(idList[0].childNodes[0].nodeValue)
            # self.logger.debug("MAX 64, id is: " + str(len(id)))
            
            #get content block
            contentList = e.getElementsByTagName('content')
            #TODO: Allow longer Content block in layers 
            try:
                nodeValue = contentList[0].childNodes[0].childNodes[0].childNodes[0].nodeValue
                content = nodeValue.encode('ascii', 'ignore')

                if (len(content) > 999):
                    self.logger.debug("MAX 1000, content is: " + str(len(content)))
                    content = content[:999]
            except:
                content = ""

            #get expiry
            expiresList = e.getElementsByTagName('age:expires')
            expires = str(expiresList[0].childNodes[0].nodeValue)
            # self.logger.debug("MAX 20, expires is: " + str(len(expires)))
            #get published
            publishedList = e.getElementsByTagName('published')
            published = str(publishedList[0].childNodes[0].nodeValue)
            # self.logger.debug("MAX 20, published is: " + str(len(published)))

            atts = {"Updated" : updated,
                    "icon" : icon,
                    "Title" : title,
                    "Content" : content,
                    "Severity" : severity,
                    "Urgency" : urgency,
                    "Expires" : expires,
                    "Published" : published,
                    "AuthorURI" : uri,
                    "AuthorName" : name,
                    "id" : id}
            
            #perform the following if a point is found
            if e.getElementsByTagName('georss:point'):
                locationList = e.getElementsByTagName('georss:point')
                latlon = locationList[0].childNodes[0].nodeValue.split()
                lat = float(latlon[0])
                lon = float(latlon[1])
                feat = {"geometry" : {"x" : lon, "y" : lat, "spatialReference" : {"wkid" : 4326}} ,
                  "attributes" : atts}
                featType = "point"

                if id in pointIds:
                    feat['attributes']['FID'] = pointIds[id]
                    updatePointFeatures.append( feat )
                    del pointIds[id]
                else:
                    addPointFeatures.append( feat )
                # end if
            #perform the following if a polygon is found
            elif e.getElementsByTagName('georss:polygon'):
                locationList = e.getElementsByTagName('georss:polygon')
                latlonList = locationList[0].childNodes[0].nodeValue.split()
                rings = []
                for i in range(0, len(latlonList), 2):
                    rings.append([json.dumps(float(latlonList[i+1])), json.dumps(float(latlonList[i]))])
                feat = {"geometry" : {
                    "rings" : [ 
                     rings
                    ],
                    "spatialReference" : {"wkid" : 4326}
                    },
                    "attributes" : atts}
                featType = "polygon"

                if id in polygonIds:
                    feat['attributes']['FID'] = polygonIds[id]
                    updatePolygonFeatures.append( feat )
                    del polygonIds[id]
                else:
                    addPolygonFeatures.append( feat )
                # end if
            elif e.getElementsByTagName('georss:line'):
                locationList = e.getElementsByTagName('georss:line')
                latlonList = locationList[0].childNodes[0].nodeValue.split()
                paths = []
                for i in range(0, len(latlonList), 2):
                    paths.append([json.dumps(float(latlonList[i+1])), json.dumps(float(latlonList[i]))])
                feat = {"geometry" : {
                    "paths" : [ 
                     paths
                    ],
                    "spatialReference" : {"wkid" : 4326}
                    },
                    "attributes" : atts}

                featType = "line"

                if id in lineIds:
                    feat['attributes']['FID'] = lineIds[id]
                    updateLineFeatures.append( feat )
                    del lineIds[id]
                else:
                    addLineFeatures.append( feat )
                # end if
            # end if
        # end for

        # Add the Features...
        result = self.arcGISmanager.SendEntry( addPointFeatures, "point" )
        result = self.arcGISmanager.SendEntry( addLineFeatures, "line" )
        result = self.arcGISmanager.SendEntry( addPolygonFeatures, "polygon" )

        # Update the Features...
        result = self.arcGISmanager.UpdateEntry( updatePointFeatures, "point" )
        result = self.arcGISmanager.UpdateEntry( updateLineFeatures, "line" )
        result = self.arcGISmanager.UpdateEntry( updatePolygonFeatures, "polygon" )

        # Delete the Features that have not been added or updated...
        result = self.arcGISmanager.DeleteEntry( pointIds.values(), "point" )
        result = self.arcGISmanager.DeleteEntry( lineIds.values(), "line" )
        result = self.arcGISmanager.DeleteEntry( polygonIds.values(), "polygon" )

    # end GetEntries
