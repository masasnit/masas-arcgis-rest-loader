import sys
import os
import json
import requests

from optparse import OptionParser
from mako.template import Template

from ArcGISManager import ArcGISManager

#load logging config
logging_config = {}
execfile("logging_config.py", {}, logging_config)

# LOGGING
import logging, logging.handlers
from logging.handlers import RotatingFileHandler


current_dir = os.path.dirname(os.path.abspath(__file__))
# create formatter and add it to the handlers
formatter = logging.Formatter(logging_config["logging_info"]["logKEY"] + ' %(asctime)s - %(levelname)s - %(message)s')

loggers = logging_config["logging_info"]["loggers"]

logger = logging.getLogger(logging_config["logging_info"]["logKEY"])
logger.setLevel( logging_config["logging_info"]["loglevel"])

# file-based log
if "file" in loggers:
    app_dir = os.path.dirname(sys.argv[0])
    log_path = os.path.join(current_dir, logging_config["logging_info"]["logfile"])
    print "Logging to file %s" % log_path

    fh = RotatingFileHandler(log_path, maxBytes=100000, backupCount=10)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

# console log
if "console" in loggers:
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    print "added console log for %s" % logging_config["logging_info"]["logKEY"]

# syslog
if "syslog" in loggers:
    # hook up the syslog.
    if "logfile" in logging_config["logging_info"]:
        sl = logging.handlers.SysLogHandler(logging_config["logging_info"]["syslogaddress"])
        sl.setLevel(logging.INFO)
        logger.addHandler(sl)


logger.info(" Starting - logger attached.")

class CreateLayersService():

    arcgis = None
    app_config = {}

    #load logging config
    def __init__( self, user, password ):
        execfile("config.py", {}, self.app_config)

        #Add loggers to settings
        self.app_config["arcgisSettings"]["logger"] = logger

        self.arcgis = ArcGISManager( self.app_config["arcgisSettings"] )
        self.arcgis.username = user
        self.arcgis.password = password
        self.arcgis.GetToken()

        return
    # end __init__

    def createFeatureService(self, featureServiceName):

        with open('ServiceDefinitions/featureServiceTemplate.json') as jsonFile:
            svcJson = json.load(jsonFile)

        svcJson['name'] = featureServiceName

        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

        params = {
            'token': self.arcgis.token,
            'createParameters': json.dumps(svcJson),
            'outputType': 'featureService',
            'f': 'json'
        }

        request = requests.post( self.app_config["arcgisSettings"]["contentEndpoint"] + '/users/' + self.app_config["arcgisSettings"]["user"] + '/createService',
                                params=params, headers=headers)
        response = request.json()
        return response

    def createLayers(self, featureServiceUrl ):
        self.CreateFeatureLayer( featureServiceUrl, "point", "point")
        self.CreateFeatureLayer( featureServiceUrl, "line", "line")
        self.CreateFeatureLayer( featureServiceUrl, "polygon", "polygon")
        return
    # end createLayers()


    # create a layer by passing in the layer information
    def CreateFeatureLayer(self, featureServiceName, featureType, layerName):
        layerData = {}
        layerInfo = {"layerName": layerName}

        if "point" in featureType:
            tempTemplate = Template(filename='./ServiceDefinitions/pointTemplate.json')
        elif "line" in featureType:
            tempTemplate = Template(filename='./ServiceDefinitions/lineTemplate.json')
        elif "polygon" in featureType:
            tempTemplate = Template(filename='./ServiceDefinitions/polygonTemplate.json')
        # end if

        url = self.app_config["arcgisSettings"]["adminService"] + "/" + featureServiceName + "/FeatureServer/addToDefinition"

        layerTemplate = tempTemplate.render(layer=layerInfo)

        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "application/json"}

        params = {
            'token': self.arcgis.token,
            #'addToDefinition': layerTemplate,
            'async': 'false',
            'f': 'pjson'
        }

        data = {
            'addToDefinition': layerTemplate
        }

        request = requests.post( url, params=params, headers=headers, data=data )
        response = request.json()

        if( 'success' in response ):
            logger.info("Created New Layer '" + layerName + "'.")
        else:
            logger.info("Error: New layer could not be created:. " + request.text)
        # endif

    # end CreateFeatureLayer()


if __name__ == '__main__':

    #usage = "Usage: createLayers [options]"

    parser = OptionParser()
    parser.add_option("-n", "--name", action="store", type="string", dest="featureServiceName", default="MASAS Service", help="Feature Service Name")
    parser.add_option("-u", "--user", action="store", type="string", dest="user", help="ArcGIS Online User")
    parser.add_option("-p", "--password", action="store", type="string", dest="password", help="ArcGIS Online Password")
    (options, args) = parser.parse_args()

    service = None

    try:
        service = CreateLayersService(options.user, options.password)
    except Exception, ex:
        print( "An error occured.")
        sys.exit()
    # end try

    svcMeta = service.createFeatureService( options.featureServiceName )
    if(svcMeta['success']):
        service.createLayers( svcMeta['name'] )
    # end if

    print( "Layers Created!")
