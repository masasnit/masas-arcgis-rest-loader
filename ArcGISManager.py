# -*- coding: utf-8 -*-
import requests

import urllib2 # TODO: REMOVE DEPENDENCY ON THIS LIB
import json
import urllib
from datetime import datetime


class ArcGISManager:
    """ handler for ArcGIS Server and AGOL communication """
   
    
    def __init__(self, settings={}):
        """ Initialize - set logger, FeatureServiceURL, account info...
        """
        if "logger" in settings:
            self.logger = settings["logger"]
        else: 
            raise # error - we need a logger.
        
        self.token = ""
        self.clientId = settings["clientId"]
        self.clientSecret = settings["clientSecret"]
        self.username = settings["user"]
        self.password = settings["password"]
        self.portal = settings["portal"]
        self.pointFeatureLayer = settings["pointFeatureLayer"]
        self.lineFeatureLayer = settings["lineFeatureLayer"]
        self.polygonFeatureLayer = settings["polygonFeatureLayer"]

        self.adminFeatureService = settings["adminFeatureService"]
        self.pointFeatureAdminLayer = str(self.pointFeatureLayer).replace('rest/services', 'rest/admin/services')
        self.lineFeatureAdminLayer = str(self.lineFeatureLayer).replace('rest/services', 'rest/admin/services')
        self.polygonFeatureAdminLayer = str(self.polygonFeatureLayer).replace('rest/services', 'rest/admin/services')

        self.logger.info("ArcGISManager initialized.")

        return None
        
    def GetToken_OAUTH(self):
        """ get token from system """

        params = {
            'client_id': self.clientId,
            'client_secret': self.clientSecret,
            'grant_type': "client_credentials"
        }

        request = requests.post( self.portal + '/sharing/oauth2/token', params=params )
        response = request.json()
        self.token = response["access_token"]
        tokenExpires = response["expires_in"]

        print "TOKEN: %s \nEXPIRES: %s " % (self.token, datetime.fromtimestamp(tokenExpires / 1000).isoformat())

        return self.token

    def GetToken(self):
        """ get token from system """
        
        parameters = urllib.urlencode({'username':self.username,'password':self.password,'client':'referer','f':'json', 'referer': 'http:\\www.whatever.com', 'expiration': 60})
        request = self.portal + '/sharing/rest/generateToken?'
        response = json.loads(urllib.urlopen(request, parameters).read())
        self.logger.debug("TOKEN Full Response: %s" % response)
        
        self.token = response['token']
        tokenExpires = response['expires']

        print "TOKEN: %s \nEXPIRES: %s " % (self.token, datetime.fromtimestamp(tokenExpires / 1000).isoformat())
        
        return self.token
    
    #Tokens have been inconsistent up to date. This function checks to see if token is valid before continuing. 
    def VerifyToken(self):
        # default to point
        layerURL = self.pointFeatureLayer
        #Query all entries to test token validity  
        url = layerURL + "query?where=FID>0&f=json&token=" + self.token
        request = urllib2.Request(url)
        request.add_header("content-type","application/x-www-form-urlencoded")
        request.add_header("Accept","application/json")
        request.add_header("token", self.token)
        push = urllib2.urlopen(request)
        result = push.read()
#         self.logger.info("Token verification. RESULT: %s" % result)
        
        if "Invalid token" in result:
            return False
        else:
            return True
        
    
    def TokenExpiredHandler(self):
        """ handle exception - get new token """
        raise NotImplementedError


    def Get( self, url, params ):
        params['f'] = 'json'
        params['token'] = self.token
        #url += "?f=json&token=" + self.token

        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        }

        r = requests.get( url, params=params, headers=headers )
        return r.json()
    # end Get()

    def Post( self, url, params, data ):

        params['f'] = 'json'
        params['token'] = self.token
        #url += "?f=json&token=" + self.token

        headers = {
            "content-type": "application/x-www-form-urlencoded",
            "Accept": "application/json"
        }

        r = requests.post( url, params=params, headers=headers, data=data )
        return r.json()
    # end Post()

    def GetIds( self, layerUrl ):
        featureIds = {}

        # Get Point layer ids...
        url = layerUrl + "query"

        params = {
            "where": "1=1",
            "outFields": "FID,id"
        }

        results = self.Get( url, params )
        for feature in results["features"]:
            featureIds[ feature["attributes"]["id"] ] = feature["attributes"]["FID"]
        # end for

        return featureIds
    # end GetIds()

    def SendEntry(self,payload, featType):
        """ send entry to the layer(s) as required.  
        RETURN: Layer and Feature IDs where data were placed"""
        
        # default to polygon
        layerURL = self.polygonFeatureLayer
        if (featType == "point"): 
            layerURL = self.pointFeatureLayer
        elif (featType == "line"):
            layerURL = self.lineFeatureLayer

        url = layerURL + "addFeatures?f=json&token=" + self.token

        try:
            headers = {
                "content-type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            }

            featuresStr = json.dumps( payload ) #, ensure_ascii=True )
            self.logger.info("Data to send to ArcGIS. RESULT: %s" % featuresStr)
            featuresStr = urllib.quote_plus(featuresStr)

            data = "features=" + featuresStr
            params = {
                'token': self.token,
                'f': 'json',
                'Features': featuresStr
            }

            r = requests.post( url, headers=headers, data=data, timeout=600 )
            self.logger.info("Data sent to ArcGIS. RESULT: %s" % r.text)

            jsonResult = r.json()

            sendResult = []
            if "error" in r.text:
                sendResult.append(-1)
                sendResult.append(False)
            else:
                sendResult.append(jsonResult["addResults"][0]["objectId"])
                sendResult.append(True)
            
            return sendResult
        except Exception as ex:
            sendResult = []
            sendResult.append(-1)
            sendResult.append(False)
            return sendResult
    # end SendEntry
    
    def UpdateEntry(self,payload, featType):
        """ send entry to the layer(s) as required.
        RETURN: Layer and Feature IDs where data were placed"""

        # default to polygon
        layerURL = self.polygonFeatureLayer
        if (featType == "point"):
            layerURL = self.pointFeatureLayer
        elif (featType == "line"):
            layerURL = self.lineFeatureLayer

        url = layerURL + "updateFeatures?f=json&token=" + self.token

        try:
            headers = {
                "content-type": "application/x-www-form-urlencoded",
                "Accept": "application/json"
            }

            featuresStr = json.dumps( payload ) #, ensure_ascii=True )
            self.logger.info("Data to send to ArcGIS. RESULT: %s" % featuresStr)
            featuresStr = urllib.quote_plus(featuresStr)

            data = "features=" + featuresStr
            params = {
                'token': self.token,
                'f': 'json',
                'Features': featuresStr
            }

            r = requests.post( url, headers=headers, data=data, timeout=600 )
            self.logger.info("Data sent to ArcGIS. RESULT: %s" % r.text)

            jsonResult = r.json()

            sendResult = []
            if "error" in r.text:
                sendResult.append(-1)
                sendResult.append(False)
            else:
                sendResult.append(jsonResult["addResults"][0]["objectId"])
                sendResult.append(True)

            return sendResult
        except Exception as ex:
            sendResult = []
            sendResult.append(-1)
            sendResult.append(False)
            return sendResult
    # end UpdateEntry()

    def DeleteEntry(self, ids, featType):
        
         # default to polygon
        layerURL = self.polygonFeatureLayer
        if (featType == "point"): 
            layerURL = self.pointFeatureLayer
        elif (featType == "line"):
            layerURL = self.lineFeatureLayer

        where = ",".join( str(x) for x in ids )
        data = {"objectIds" : where}  #add where statement
        
        #Delete the specified entry
        entryURL = layerURL + "deleteFeatures?f=json&token=" + self.token
        request = urllib2.Request(entryURL)
        request.add_header("content-type","application/x-www-form-urlencoded")
        request.add_header("Accept","application/json")
        request.add_header("token", self.token)
        entryDelete = urllib2.urlopen(entryURL, urllib.urlencode(data))
        request = json.load(entryDelete)
        self.logger.debug("Item Deleted: %s" % request)
    # end DeleteEntry()

    def RemoveExpiredEntries(self):
     
        i = datetime.now()
        expiresNow = "Expires<'" + str(i.strftime("%m/%d/%Y %H:%M:%S %p")) + "'"
        data = {"where" : expiresNow} #add where statement
        
        #delete all the expired points
        pointURL = self.pointFeatureLayer + "deleteFeatures?f=json&token=" + self.token
        request = urllib2.Request(pointURL)
        request.add_header("content-type","application/x-www-form-urlencoded")
        request.add_header("Accept","application/json")
        request.add_header("token", self.token)
        pointDelete = urllib2.urlopen(pointURL, urllib.urlencode(data))
        request = json.load(pointDelete)
        self.logger.debug("List of Points deleted: %s" % request)
        
        
        #delete all the expired lines
        pointURL = self.lineFeatureLayer + "deleteFeatures?f=json&token=" + self.token
        request = urllib2.Request(pointURL)
        request.add_header("content-type","application/x-www-form-urlencoded")
        request.add_header("Accept","application/json")
        request.add_header("token", self.token)
        pointDelete = urllib2.urlopen(pointURL, urllib.urlencode(data))
        request = json.load(pointDelete)
        self.logger.debug("List of Lines deleted: %s" % request)
        
        #delete all the expired polygons
        pointURL = self.polygonFeatureLayer + "deleteFeatures?f=json&token=" + self.token
        request = urllib2.Request(pointURL)
        request.add_header("content-type","application/x-www-form-urlencoded")
        request.add_header("Accept","application/json")
        request.add_header("token", self.token)
        pointDelete = urllib2.urlopen(pointURL, urllib.urlencode(data))
        request = json.load(pointDelete)
        self.logger.debug("List of Polygons deleted: %s" % request)
    
    def ClearOutLayers(self):
        """
        Delete all features in layers.
        """

        # Get the headers and params ready...
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "application/json"}

        params = {
            'token': self.token,
            'attachmentOnly': 'false',
            'async': 'false',
            'f': 'json'
        }

        request = requests.post(self.pointFeatureAdminLayer + '/truncate', params=params, headers=headers)
        response = request.json()
        if( 'success' in response and response['success'] is True ):
            print( "Point Features Truncated.")

        request = requests.post(self.lineFeatureAdminLayer + '/truncate', params=params, headers=headers)
        response = request.json()
        if ('success' in response and response['success'] is True):
            print("Line Features Truncated.")

        request = requests.post(self.polygonFeatureAdminLayer + '/truncate', params=params, headers=headers)
        response = request.json()
        if ('success' in response and response['success'] is True):
            print("Polygon Features Truncated.")

        # Refresh the caches...
        request = requests.post(self.pointFeatureAdminLayer + '/refresh', params=params, headers=headers)
        response = request.json()
        if ('success' in response and response['success'] is True):
            print("Point Features Refreshed.")

        request = requests.post(self.lineFeatureAdminLayer + '/refresh', params=params, headers=headers)
        response = request.json()
        if ('success' in response and response['success'] is True):
            print("Line Features Refreshed.")

        request = requests.post(self.polygonFeatureAdminLayer + '/refresh', params=params, headers=headers)
        response = request.json()
        if ('success' in response and response['success'] is True):
            print("Point Features Refreshed.")

        request = requests.post(self.adminFeatureService + '/refresh', params=params, headers=headers)
        response = request.json()
        if ('success' in response and response['success'] is True):
            print("Feature Service Refreshed.")

    # end ClearOutLayers()

