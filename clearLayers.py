import sys
import os

from optparse import OptionParser

from ArcGISManager import ArcGISManager

#load logging config
logging_config = {}
execfile("logging_config.py", {}, logging_config)

# LOGGING
import logging, logging.handlers
from logging.handlers import RotatingFileHandler


current_dir = os.path.dirname(os.path.abspath(__file__))
# create formatter and add it to the handlers
formatter = logging.Formatter(logging_config["logging_info"]["logKEY"] + ' %(asctime)s - %(levelname)s - %(message)s')

loggers = logging_config["logging_info"]["loggers"]

logger = logging.getLogger(logging_config["logging_info"]["logKEY"])
logger.setLevel( logging_config["logging_info"]["loglevel"])

# file-based log
if "file" in loggers:
    app_dir = os.path.dirname(sys.argv[0])
    log_path = os.path.join(current_dir, logging_config["logging_info"]["logfile"])
    print "Logging to file %s" % log_path

    fh = RotatingFileHandler(log_path, maxBytes=100000, backupCount=10)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

# console log
if "console" in loggers:
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    print "added console log for %s" % logging_config["logging_info"]["logKEY"]

# syslog
if "syslog" in loggers:
    # hook up the syslog.
    if "logfile" in logging_config["logging_info"]:
        sl = logging.handlers.SysLogHandler(logging_config["logging_info"]["syslogaddress"])
        sl.setLevel(logging.INFO)
        logger.addHandler(sl)


logger.info(" Starting - logger attached.")

class ClearLayersService():

    arcgis = None
    app_config = {}

    #load logging config
    def __init__( self, user, password ):
        execfile("config.py", {}, self.app_config)

        #Add loggers to settings
        self.app_config["arcgisSettings"]["logger"] = logger

        self.arcgis = ArcGISManager( self.app_config["arcgisSettings"] )
        self.arcgis.username = user
        self.arcgis.password = password
        self.arcgis.GetToken()

        return
    # end __init__

    def clearLayers(self):
        self.arcgis.ClearOutLayers()
        return
    # end createLayers()


if __name__ == '__main__':

    #usage = "Usage: createLayers [options]"

    parser = OptionParser()
    parser.add_option("-u", "--user", action="store", type="string", dest="user", help="ArcGIS Online User")
    parser.add_option("-p", "--password", action="store", type="string", dest="password", help="ArcGIS Online Password")
    (options, args) = parser.parse_args()

    service = None

    try:
        service = ClearLayersService(options.user, options.password)
    except Exception, ex:
        print( "An error occured.")
        sys.exit()
    # end try

    svcMeta = service.clearLayers()

    print( "Layers Cleared!")
